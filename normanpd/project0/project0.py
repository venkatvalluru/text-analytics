import urllib.request
import PyPDF2
import tempfile
import sqlite3
from sqlite3 import Error

def fetchincidents(url):
     return urllib.request.urlopen(url).read()

def extractincidents(data):
    fp = tempfile.TemporaryFile()

    # Write the pdf data to a temp file
    fp.write(data)

    # Set the curser of the file back to the begining
    fp.seek(0)

    # Read the PDF
    pdfReader = PyPDF2.PdfFileReader(fp)
    pdfReader.getNumPages()

    # Get the first page
    page1 = pdfReader.getPage(0).extractText()
    page1 = page1.replace('Officer', 'Officer;').replace(' \n',' ').replace('-\n','-').replace('\n','$')
    page1=page1.replace('$D - DUS','D - DUS').split(";")

    i=0
    while i<len(page1):
        page1[i]=page1[i].strip('$')
        i=i+1

    j=0
    while j<len(page1):
        page1[j]=page1[j].split("$")
        j+=1

    page1.pop(len(page1)-1)

    k=0
    while k<len(page1):
        if len(page1[k])==12:
            for m in range(0,len(page1[k])):
                if m==6:
                    page1[k][m]=''.join(page1[k][m:m+4])
                    #print(page1[k][m])
                    #print(len(page1[k]))
                    page1[k].pop(m+1)
                    page1[k].pop(m+1)
                    page1[k].pop(m+1)
        elif len(page1[k])==11:
            for n in range(0,len(page1[k])):
                if n==6:
                    page1[k][n]=''.join(page1[k][n:n+3])
                    page1[k].pop(n+1)
                    page1[k].pop(n+1)
        elif len(page1[k])==10:
            for p in range(0,len(page1[k])):
                if p==6:
                    page1[k][p] = ''.join(page1[k][p:p + 2])
                    page1[k].pop(p+1)
        k+=1
    del page1[0]
    return page1

def createdb():
    try:
        connection_object=sqlite3.connect('normanpd.db')
        delete_tab = connection_object.cursor()
        delete_tab.execute("Drop table if exists arrests")
        data_object=connection_object.cursor()
        data_object.execute("""CREATE TABLE IF NOT EXISTS  arrests (
     arrest_time TEXT,
    case_number TEXT,
    arrest_location TEXT,
    offense TEXT,
    arrestee_name TEXT,
    arrestee_birthday TEXT,
    arrestee_address TEXT,
    status TEXT,
    officer TEXT)""")
        connection_object.commit()
    except Error as e:
        print(e)
    finally:
        connection_object.close()
    return 'normanpd.db'

def populatedb(db,a):
    #print(a)
    connection_object=sqlite3.connect('normanpd.db')
    data_object = connection_object.cursor()
    for data in range(len(a)):
        data_object.execute('insert into arrests values(?,?,?,?,?,?,?,?,?)', a[data])
        connection_object.commit()
    connection_object.close()

def status(db):
    connection_object = sqlite3.connect('normanpd.db')
    data_object = connection_object.cursor()
    data_object.execute("select * from arrests order by random() limit 1")
    retrieve_data=data_object.fetchall()
    for q in retrieve_data:
        print("þ".join(q))
    return ("þ".join(q))


