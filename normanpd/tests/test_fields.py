import pytest
import sqlite3

from project0 import project0
myurl='http://normanpd.normanok.gov/filebrowser_download/657/2019-02-14%20Daily%20Arrest%20Summary.pdf'

def test_extractincidents():
    text = project0.fetchincidents(myurl)
    testdf=project0.extractincidents(text)
    assert len(testdf[0]) == 9


db = "normanpd.db"
def test_createdb():
    dbname=project0.createdb()
    assert dbname == db


def test_populatedb():
    sql = sqlite3.connect(db)
    cur = sql.cursor()
    cur.execute('select * from arrests order by random() limit 1')
    result = cur.fetchall()
    assert result is not None

def test_pop():
    a = [['2/17/2019 2:00', '2019-00013301', 'E BOYD ST / CLASSEN BLVD', 'DUI UNDER 21', "KIARA O'BRIEN", '3/7/2000', '1403 SW 20TH STMOOREOK73160', 'FDBDC (Jail)', '1642 - Vu']]
    project0.populatedb("normanpd.db",a)
    sql = sqlite3.connect(db)
    cur = sql.cursor()
    result = project0.status(db)
    assert str(result) == "2/17/2019 2:00þ2019-00013301þE BOYD ST / CLASSEN BLVDþDUI UNDER 21þKIARA O'BRIENþ3/7/2000þ1403 SW 20TH STMOOREOK73160þFDBDC (Jail)þ1642 - Vu" 

def test_status():
    randomrow=project0.status(db)
    assert type(randomrow)== str

