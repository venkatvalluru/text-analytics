# PROJECT0

# Author
**VALLURU VENKAT SAI SRIHARSHA**

Author mailid: valluru@ou.edu

Packages used: urllib,sqlite3,PyPDF2,argparse,tempfile 

---

# Running code: 
pipenv run python project0/main.py --arrests <url>

---

# Running test cases:
pipenv run python -m pytest

---

# Description:
The main objective of the project is to clean up the data contained in http://normanpd.normanok.gov/content/daily-activity. The main idea of this project is Web Scrapping. We are extracting the information that is necessary for us in a few lines of code. A  gentle reminder is that not all data should be scrapped.

---

# FETCHINCIENTS:

The objective of this task is to download the data from the url that is provided. The data will be in the form of HTML and we need to prettify it. To accomplish this task we made use urllib.request library where we are able to open the url and download the data from the url. Further operations are performed on the data object in which the content from the url is stored.

---

# EXTRACTINCIDENTS:

The objective of this task is to identify the leading characters , extracting the data and cleaning the data. So first thing that we need to do is to bring the data from the first function i.e extractincidents(). The goal of our project is to extract the information of only the Arrest summary. The information that is read and downloaded in the first function read by using a PyPDF2 module which has PdfFileReader function to read the data. Next our task is to clean the data and we first do it by observing a common similarity in the data.
Except the first row every row is seprated by a semi-colon and we use the replace() function to put a semi colon after the first row i.e exactly after Officer.The leading characters after the comma are trimmed by using strip(). The header of the page is also appearing in the output so we delete it by using the pop function. Next we iterate and clean the data and concatenate rows as according to the database requirements.

---

# CREATEDB:

Here our aim is to create a database. We are using sqllite3 for this project. First a connection is established with the database and we do that by using connect() method.Next we create the table with the given data fields.In case of any any exception we are using a try,catch,finally to catch the exception.So the connection object is closed under the finally block.

---
					
# POPULATEDB:
The task of this function is to populate the database and insert the values into the database. We use the iterator to run the script.

---

# STATUS:

The objective of this function is to generate a random row from the database. We use the random()  to accomplish this task. The fetchall() generates all the rows from the database.

---


# Assumptions:
The assumption that I made is that this code is valid for only forn the PDF’S that structured in the same way as Norman PDF files.
If the url given in the test file expires the cases might not work. So which ever url we use to run the code that url should be given in the test case file.

---

<pre>
.
├── COLLABORATORS
├── LICENSE
├── Pipfile
├── Pipfile.lock
├── README.md
├── docs
├── normanpd.db
├── project0
│   ├── main.py
│   └── project0.py
├── setup.cfg
├── setup.py
└── tests
    ├── test_download.py
    └── test_fields.py
<pre>

