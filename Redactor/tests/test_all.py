import project1
from project1 import redactor

def test_names():
	text = "My name is Harsha. My phone number is (+1)405-289-9090. My best friend is Bhavana"
	y,Y= redactor.names_redact(text)
	assert Y > 0

def test_numbers_redact():

    text = "My name is Harsha. My phone number is (+1)405-289-9090. My best friend is Bhavana"
    s,S=redactor.numbers_redact(text)
    assert S > 0

def test_dates_readct():
    text = "My name is Harsha. My phone number is (+1)405-289-9090. My best friend is Bhavana"
    v,V=redactor.dates_redact(text)
    assert V > 0




