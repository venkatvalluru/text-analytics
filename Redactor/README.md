# cs5293sp19-project1

Name:Venkat Sai Sriharsha Valluru
email:valluru@ou.edu
The objective of the project is to redact sensitive data like names,genders,locations,concepts. In brief first  I extracted all the data that I require using  nltk(Natural Language Toolkit) and spacy.Natural language processing is the means by which computers are made to understand human languages like the words that we speak daily.
File opening:
In the first function we open a file in read mode and read the data from the text files.I used the file opening format as with open() as fp. So   if we use this method to perform operations in a file everything that we write has to be under this function. This way of opening a file also reduces the operation of closing a file because this way of opening a file closes the file automatically as we come out of the block.
Redacting names:
In this function we are redacting the names by using nltk.First we do this by performing word_tokenize which is a beautiful function available to tokenize a text  into words. Then we perform tokenize operation by using “parts of speech” tag.Then we bring every name under a chunk of ” PERSON” by using ne_chunk function.Then I replaced the label with PERSON with a blank character.
Redacting gender:
In this function we are redacting genders.I first took every possible name of a gender for e.g him,her,he,she into a list. Next I used “re.compile()” with a regular expression and used the pattern to search in the text.\\b is used so that words like he,she are replaced individually without replaced being in any word .Then the genders are searched in the text using findall.Then the genders are redacted.
Redacting address:
This function helps us to redact addresses.I used a regular expression to find all the addresses that come under that regular expression.Next I used the label of ‘GPE’ tpo extract the locatins in a given text file.First we perform word tokenization and then apply pos_tag on them and use ne_chunk to chunk every entity that is a location.The extracted addresses are then redacted,
Redacting dates:
To redact dates we made use of ‘Spacy’ parser to extract the dates. So the dates will be redacted along with months,weeks,years etc.To make use of this parser first we need to load ‘en’ in your machine.Then after extracting the dates we redact the dates present.
Redacting concepts:
The given word and its synonyms need to be redacted. For example there is a word called ‘run’.Every word related to run has to be redacted. In this I made use of “wordnet” to get the synonyms of the given word. I have done this by creating an object of  WordNetLemmatizer.We can wordnet by using from nltk.corpus import wordnet.Then I lemmatized the text .If the synonyms are present in the text then I redacted them.To redact a sentence I found a word and if the word is present in the sentence I redacted the sentence.
The stats get a count of the number of entities redacted.
How to run:
pipenv run python redactor.py --input '*.txt' \
                    --input 'otherfiles/*.txt' \
                    --names --dates --addresses --phones \
                    --concept 'kids' \
                    --output 'files/' \
                    --stats stderr

Assumptions:
1)	When I use the label ‘PERSON’ to chunk the names I also get the names of a few entities  other than the names if it is a Pronoun.  The word Saffron gets redacted in the word Saffron Park.The names like Deonne that specify address also get redacted.
2)	The regular expression for  phone numbers works only for nthe numbers within the United States.
3)	When redacting dates if a number in the text is of the form 1013 or 1014 the number gets redacted which is also an assumption.
4)	The regular expression for addresses works for only those type of addresses.
References:
1)	https://www.youtube.com/channel/UCfzlCWGWYyIQ0aLC5w48gBQ - made use of these tutorials to get the knowledge of Stemming,Lemmatization.
2)	https://stackoverflow.com/questions/31836058/nltk-named-entity-recognition-to-a-python-list -chunking using ne_chunk.
3)	 https://github.com/vishnuvikash/Redactor-Unredactor -gained an idea about the dates and genders.

