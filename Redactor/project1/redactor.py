import nltk
from nltk import ne_chunk
import re
import sys
import os
import glob
import pyap
import spacy
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer,WordNetLemmatizer
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('punkt')
count_names = 0
count_gender = 0
count_dates = 0
count_numbers = 0
count_address = 0
c=[]
d=[]
m=[]
o=[]
n=[]
addr=[]
ad=[]
synonyms=[]
addresses=[]
ki=[]

def file_open(file_index):
    with open(file_index, 'r') as f:
        b = f.read()
        # print(b)
        #for random in range(0, len(b)):
            #b[random] = b[random].strip()
        return(b)

def names_redact(a):
    count_names = 0
    sample_sent=nltk.word_tokenize
    sent=sample_sent(a)
    #print(sent)
    parts=nltk.pos_tag(sent)
    for chunk in nltk.ne_chunk(parts):
        if hasattr(chunk,'label'):
            if chunk.label()=='PERSON':
                c.append(' '.join(chunk[0] for chunk in chunk.leaves()))
    #print(c)
    for each in c:
        count_names = count_names + a.count(each)
        a=a.replace(each,'████')
    #print(count_names)
    #print("names redacted")
    #print(a)
    return(a,count_names)

def gender_redact(y):
    pattern=re.compile('\\b'+r'(him|his|her|he|she|man|woman|male|female|himself|herself)'+'\\b',re.IGNORECASE)
    r=pattern.findall(y,re.IGNORECASE)
    count_gender = 0
    count_gender = len(r)
    for s in r:
        y=re.sub('\\b'+s+'\\b','█'*len(s),y)
    #print(count_gender)
    #print("genders redacted")
    #print(y)
    return(y,count_gender)

def redact_address(z):
    caught=[]
    c1=[]
    count_gender = 0
    pattern=re.compile(r'\d{1-4}\s[A-Z]\w+\s[A-Z]\w+.[A-Z]\w+.[A-Z]{2}.\d{5}',re.IGNORECASE)
    find=pattern.findall(z,re.IGNORECASE)
    #print(find)
    for fi in find:
        count_gender = count_gender + z.count(fi)
        z=z.replace(fi,'████')
    for chunk1 in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(z))):
        if hasattr(chunk1,'label'):
            if chunk1.label()=='GPE':
                c1.append(' '.join(chunk1[0] for chunk1 in chunk1.leaves()))
    #print(c1)
    for any in c1:
        count_gender = count_gender + z.count(any)
        z=z.replace(any,'████')
    return(z, count_gender)

def dates_redact(i):
    nlp = spacy.load('en') 
   #nlp = spacy.load('en')
    doc=nlp(i)
    for ent in doc.ents:
        if hasattr(ent,'label') and ent.label_=='DATE':
            m.append(ent.text)

   #print(m)
    count_dates = 0
    for some_rand in m:
        #print(some_rand)
        count_dates = count_dates + i.count(some_rand)
        i=i.replace(some_rand,'████')

    #print(count_dates)
    #print("dates redacted")
    #print(z)
    return(i,count_dates)

def numbers_redact(v):
    regexp=re.compile(r'\(\+1\)\d\d\d\-\d\d\d\-\d\d\d\d')
    found1=regexp.findall(v)
    for found_each in found1:
        o.append(found_each)
    #print(o)
    count_numbers = 0
    for list_o in o:
        count_numbers = count_numbers + v.count(list_o)
        v=v.replace(list_o,'████')
    #print(count_numbers)
    return(v,count_numbers)

def redact_concept(s,concept):
    sysns=wordnet.synsets(str(concept))
    #reg=re.compile('\\b'+r'children'+'\\b',re.IGNORECASE)
    #pattern_found=reg.findall(v,re.IGNORECASE)
    #for pat in pattern_found:
     #   v=re.sub('\\b'+pat+'\\b','█'*len(pat),v,re.IGNORECASE)
    #print(pattern_found)
    #print(v)
    for syn in sysns:
        for l in syn.lemmas():
            synonyms.append(l.name())
    #print(set(synonyms))
    lemm = WordNetLemmatizer()
    #for sent in nltk.sent_tokenize(v):
    for words in nltk.word_tokenize(s):
        for syn1 in synonyms:
            if syn1 in lemm.lemmatize(words):
                s=s.replace(words,lemm.lemmatize(words))
    #print(v)

    for sent in nltk.sent_tokenize(s):
        for word in nltk.word_tokenize(sent):
            for syn2 in synonyms:
                if syn2 in word:
                    s=s.replace(sent,'████████████')

    #print(s)
    return s
def redact_stats(Y,Z,I,V,S):
    
    print("names redacted: ",Y)
    print("gender redacted: ",Z)
    print("addresses redacted: ",I)
    print("dates redacted: ",V)
    print("phone numbers redacted: ",S)


def main():
    #print("in main")
    #print(sys.argv)
    txtfiles = []
    txtfiles1 = []
    concept = ''
    stats_file = ''
    for i in range(len(sys.argv)):
        #print(sys.argv[i])
        if (sys.argv[i] == '--input'):
            if (sys.argv[i + 1] == 'otherfiles/*.txt'):
                txtfiles = glob.glob(sys.argv[i + 1], recursive=True)
            if (sys.argv[i + 1] == '*.txt'):
                txtfiles1 = glob.glob(sys.argv[i + 1], recursive = True)
            txtfiles = txtfiles + txtfiles1
        if(sys.argv[i] == '--concept'):
            concept = sys.argv[i+1]
        if(sys.argv[i] == '--stats'):
            stats_file = sys.argv[i+1]
    #print(txtfiles)
    for file_index in txtfiles:
        a = file_open(file_index)
        y,Y= names_redact(a)
        z,Z=gender_redact(y)
        i,I=redact_address(z)
        v,V=dates_redact(i)
        s,S=numbers_redact(v)
        l1=redact_concept(s,concept)
        redact_stats(Y, Z, I, V, S)
        #print("output text")
        #print(l1)
        if(len(file_index.split('/')) > 1):
            file_split = file_index.split('/')
            file_index = file_split[1]
        output_file = 'files/' + str(file_index) + '.redacted'
        with open(output_file,'w') as f:
            f.write(l1)

main()
